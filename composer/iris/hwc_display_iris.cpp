/*
 * Copyright (c) 2012-2013, 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright (C) 2020, Pixelworks, Inc.
 * These files include source code that is provided
 * under the terms and conditions of a Master Software License Agreement and
 * its applicable schedules.
 */

#if defined(PXLW_IRIS)
HWC2::Error HWCColorMode::GetColorTransform(float *matrix) {
  uint32_t i;
  for (i = 0; i < kColorTransformMatrixCount; i++) {
    matrix[i] = color_matrix_[i];
  }
  return HWC2::Error::None;
}

HWC2::Error HWCDisplay::MoveLayer(hwc2_layer_t layer_id, HWCDisplay *dest)
{
  const auto map_layer = layer_map_.find(layer_id);
  if (map_layer == layer_map_.end()) {
    DLOGE("[%" PRIu64 "] MoveLayer(%" PRIu64 ") failed: no such layer", id_, layer_id);
    return HWC2::Error::BadLayer;
  }
  const auto layer = map_layer->second;
  layer_map_.erase(map_layer);
  const auto z_range = layer_set_.equal_range(layer);
  bool found = false;
  for (auto current = z_range.first; current != z_range.second; ++current) {
    if (*current == layer) {
      current = layer_set_.erase(current);
      found = true;
      break;
    }
  }
  if (!found)
    DLOGE("layer %" PRIu64 " not found in set", layer_id);
  geometry_changes_ |= GeometryChanges::kRemoved;
  validated_ = false;
  layer_stack_invalid_ = true;

  dest->layer_set_.insert(layer);
  dest->layer_map_.emplace(std::make_pair(layer->GetId(), layer));
  dest->geometry_changes_ |= GeometryChanges::kAdded;
  dest->validated_ = false;
  dest->layer_stack_invalid_ = true;

  DLOGV("%d -> %d: id %" PRIu64, type_, dest->type_, layer_id);
  return HWC2::Error::None;
}

HWC2::Transform HWCDisplay::LayerTransform2Transform(LayerTransform layer_transform) {
  HWC2::Transform transform = HWC2::Transform::None;

  if (layer_transform.rotation == 90.0f)
    transform = HWC2::Transform::Rotate90;

  if (layer_transform.flip_horizontal) {
    transform = HWC2::Transform::FlipH;
    if (layer_transform.flip_vertical) {
      transform = HWC2::Transform::Rotate180;
      if (layer_transform.rotation == 90.0f)
        transform = HWC2::Transform::Rotate270;
    } else {
      if (layer_transform.rotation == 90.0f)
        transform = HWC2::Transform::FlipHRotate90;
    }
  }

  if (layer_transform.flip_vertical) {
    transform = HWC2::Transform::FlipV;
    if (layer_transform.rotation == 90.0f)
      transform = HWC2::Transform::FlipVRotate90;
  }

  return transform;
}

HWC2::Error HWCDisplay::MoveLayers(HWCDisplay *dest, hwc2_layer_t except_layer_id)
{
  bool nullLayer = except_layer_id == 0ULL;
  const auto except_layer = layer_map_.find(except_layer_id);
  DLOGI(" MoveLayers: %" PRIu64 , except_layer_id);
  for (auto it = layer_map_.begin(), next = it; it != layer_map_.end(); it = next) {
    next = it;
    ++next;
    if (it->first != except_layer_id &&
        (nullLayer || (it->second->GetZ() >= except_layer->second->GetZ()))) {
      DLOGI(" move: %" PRIu64 " z %d", it->first, it->second->GetZ());
      HWC2::Error status = MoveLayer(it->first, dest);
      if (status != HWC2::Error::None)
        return status;
#if 1
      iris_wrapper_->ChangeLayerType(it->first, dest->id_, dest->type_);
#else
      if (sIrisService) {
        if (type_ == kPrimary && id_ == HWC_DISPLAY_PRIMARY) {
          sIrisService->DestroyLayer(it->first);
        } else if (type_ == kPrimary && id_ == qdutils::DISPLAY_BUILTIN_2) {
          sIrisService->CreateLayer(it->first);
          sIrisService->SetLayerZOrder(it->first, it->second->GetZ());
          sIrisService->SetLayerCompositionType(it->first, (int32_t)it->second->GetDeviceSelectedCompositionType());
          Layer *layer = it->second->GetSDMLayer();
          if (layer) {
            hwc_rect_t frame = {};
            frame.right = layer->dst_rect.right;
            frame.left = layer->dst_rect.left;
            frame.bottom = layer->dst_rect.bottom;
            frame.top = layer->dst_rect.top;
            sIrisService->SetLayerDisplayFrame(it->first, frame);

            LayerTransform layer_transform = layer->transform;
            sIrisService->SetLayerTransform(it->first, (int32_t)LayerTransform2Transform(layer_transform));

            const private_handle_t *handle =
                reinterpret_cast<const private_handle_t *>(layer->input_buffer.buffer_id);
            if (handle) {
              sIrisService->SetLayerBuffer(it->first, (buffer_handle_t)handle, layer->input_buffer.acquire_fence_fd);
            }
          }
        }
      }
#endif
    } else {
      DLOGI(" keep: %" PRIu64 " z %d", it->first, it->second->GetZ());
    }
  }

  iris_wrapper_->SetLayerSetEmpty(id_, type_, layer_set_.empty());
  iris_wrapper_->SetLayerSetEmpty(dest->id_, dest->type_, dest->layer_set_.empty());
  return HWC2::Error::None;
}

HWC2::Error HWCDisplay::MoveLayersByZ(HWCDisplay *dest, uint32_t zorder, bool dir)
{
  bool move;
  for (auto it = layer_map_.begin(), next = it; it != layer_map_.end(); it = next) {
    next = it;
    ++next;
    move = dir ? (it->second->GetZ() < zorder) : (it->second->GetZ() > zorder);
    if (move) {
      DLOGI(" move: %" PRIu64 " z %d", it->first, it->second->GetZ());
      HWC2::Error status = MoveLayer(it->first, dest);
      if (status != HWC2::Error::None)
        return status;
#if 1
      iris_wrapper_->ChangeLayerType(it->first, dest->id_, dest->type_);
#else
      if (sIrisService) {
        if (type_ == kPrimary && id_ == HWC_DISPLAY_PRIMARY) {
          sIrisService->DestroyLayer(it->first);
        } else if (type_ == kPrimary && id_ == qdutils::DISPLAY_BUILTIN_2) {
          sIrisService->CreateLayer(it->first);
          sIrisService->SetLayerZOrder(it->first, it->second->GetZ());
          sIrisService->SetLayerCompositionType(it->first, (int32_t)it->second->GetDeviceSelectedCompositionType());
          Layer *layer = it->second->GetSDMLayer();
          if (layer) {
            hwc_rect_t frame = {};
            frame.right = layer->dst_rect.right;
            frame.left = layer->dst_rect.left;
            frame.bottom = layer->dst_rect.bottom;
            frame.top = layer->dst_rect.top;
            sIrisService->SetLayerDisplayFrame(it->first, frame);

            LayerTransform layer_transform = layer->transform;
            sIrisService->SetLayerTransform(it->first, (int32_t)LayerTransform2Transform(layer_transform));

            const private_handle_t *handle =
                reinterpret_cast<const private_handle_t *>(layer->input_buffer.buffer_id);
            if (handle) {
              sIrisService->SetLayerBuffer(it->first, (buffer_handle_t)handle, layer->input_buffer.acquire_fence_fd);
            }
          }
        }
      }
#endif
    } else {
      DLOGI(" keep: %" PRIu64 " z %d", it->first, it->second->GetZ());
    }
  }

  iris_wrapper_->SetLayerSetEmpty(id_, type_, layer_set_.empty());
  iris_wrapper_->SetLayerSetEmpty(dest->id_, dest->type_, dest->layer_set_.empty());
  return HWC2::Error::None;
}

bool HWCDisplay::HasLayer(hwc2_layer_t layer_id)
{
  return (layer_map_.count(layer_id) != 0);
}

bool HWCDisplay::LayerIsEmpty(HWCDisplay *dest)
{
  return dest->layer_set_.empty();
}

bool HWCDisplay::IsLayerBufferChanged(hwc2_layer_t layer_id, buffer_handle_t buffer)
{
  if (!HasLayer(layer_id))
    return false;
  const auto map_layer = layer_map_.find(layer_id);
  const auto hwc_layer = map_layer->second;
  Layer *layer = hwc_layer->GetSDMLayer();
  LayerBuffer *layer_buffer = &layer->input_buffer;
  return (layer_buffer->buffer_id != reinterpret_cast<uint64_t>(buffer));
}

void HWCDisplay::SetMoveLayersDelay(bool delay) {
  move_layers_delay_ = delay;
}

HWC2::Composition HWCDisplay::GetLayerDeviceSelectedCompositionType(hwc2_layer_t layer_id) {
	if (!HasLayer(layer_id))
		return HWC2::Composition::Invalid;
	const auto map_layer = layer_map_.find(layer_id);
	const auto hwc_layer = map_layer->second;
	return hwc_layer->GetDeviceSelectedCompositionType();
}

HWC2::Composition HWCDisplay::GetLayerClientRequestedCompositionType(hwc2_layer_t layer_id) {
	if (!HasLayer(layer_id))
		return HWC2::Composition::Invalid;
	const auto map_layer = layer_map_.find(layer_id);
	const auto hwc_layer = map_layer->second;
	return hwc_layer->GetClientRequestedCompositionType();
}

bool HWCDisplay::IsGPUTargetComp() {
	return display_intf_ && display_intf_->IsGPUTargetComp();
}

int32_t HWCDisplay::GetLayerZOrder(hwc2_layer_t layer_id) {
  if (!HasLayer(layer_id))
    return -1;
  const auto map_layer = layer_map_.find(layer_id);
  return map_layer->second->GetZ();
}

bool HWCDisplay::hasClientComposition() {
  return has_client_composition_;
}

HWC2::Error HWCDisplay::CopyColorTransform(HWCDisplay *dest) {
  if (type_ == kPrimary) {
    color_mode_->GetColorTransform(color_matrix);
  }
  dest->SyncColorTransform(color_matrix);
  return HWC2::Error::None;
}

HWC2::Error HWCDisplay::SyncColorTransform(float *matrix) {
  double color_matrix[kColorTransformMatrixCount] = {0};
  for (int i = 0; i < kColorTransformMatrixCount; i++) {
    color_matrix[i] = static_cast<double>(matrix[i]);
  }
  display_intf_->SyncColorTransform(kColorTransformMatrixCount, color_matrix);
  return HWC2::Error::None;
}

void HWCDisplay::CreateBWLayer() {
  // PxlwIrisWrapper::init() is not called, yet.
  if (iris_wrapper_->HasIrisDual()) {
    if (type_ == kPrimary) {
      // uint32_t active_config = 0;
      // DisplayConfigVariableInfo var_info = {};
      // GetActiveDisplayConfig(&active_config);
      // GetDisplayAttributesForConfig(INT32(active_config), &var_info);
      // bw_layer_ = new HWCBWCheckerLayer(buffer_allocator_, var_info.x_pixels, var_info.y_pixels,
      //                                     id_ == HWC_DISPLAY_PRIMARY);
      uint32_t primary_width = 1080;
      uint32_t primary_height = 2376;
      GetFrameBufferResolution(&primary_width, &primary_height);
      bw_layer_ = new HWCBWCheckerLayer(buffer_allocator_, primary_width, primary_height,
                                        id_ == HWC_DISPLAY_PRIMARY);
      if (!bw_layer_->Init()) {
        bw_layer_.clear();
      }
    }
  }
}

bool HWCDisplay::CheckMoveLayersDelay(uint32_t *out_num_elements) {
  if (iris_wrapper_->HasIrisDual()) {
    *out_num_elements = 0;
    if (!move_layers_delay_) {
      return true;
    }
  } else {
    return true;
  }

  return false;
}

bool HWCDisplay::CheckCommitLayerStack() {
  if (iris_wrapper_->HasIrisDual()) {
    if (shutdown_pending_) {
      DLOGD_IF(kTagClient, "Display %lu is shutdown_pending_", id_);
      return true;
    }
    if (layer_set_.empty()) {
      DLOGD_IF(kTagClient, "Display %lu is empty", id_);
      if (!move_layers_delay_) {
        return true;
      }
    }
  } else if (shutdown_pending_ || layer_set_.empty()) {
    return true;
  }

  return false;
}
#endif //PXLW_IRIS
