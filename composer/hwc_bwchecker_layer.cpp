/*
 * Copyright (c) 2012-2013, 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright (C) 2020, Pixelworks, Inc.
 * These files include source code that is provided
 * under the terms and conditions of a Master Software License Agreement and
 * its applicable schedules.
 */

#if defined(PXLW_IRIS)
#include <utils/debug.h>
#include <utils/formats.h>
#include <utils/sys.h>
#include <cutils/properties.h>

#include "hwc_bwchecker_layer.h"
#include "hwc_debugger.h"

#include <PxlwIrisWrapper.h>

#define __CLASS__ "HWCBWCheckerLayer"

namespace sdm {

using pxlw::PxlwIrisWrapper;

HWCBWCheckerLayer::HWCBWCheckerLayer(BufferAllocator *buffer_allocator,
                                     uint32_t width, uint32_t height, bool primary)
  : buffer_allocator_(buffer_allocator), width_(width), height_(height), primary_(primary)
{
}

HWCBWCheckerLayer::~HWCBWCheckerLayer()
{
  Release();
}

bool HWCBWCheckerLayer::Init()
{
  return InitInternal();
}

bool HWCBWCheckerLayer::SetFrameBufferResolution(uint32_t width, uint32_t height)
{
  width_ = width;
  height_ = height;
  Release();
  return InitInternal();
}

void HWCBWCheckerLayer::Release()
{
  if (layer_) {
    delete layer_;
    layer_ = NULL;
  }
  if (buffer_info_.private_data)
    buffer_allocator_->FreeBuffer(&buffer_info_);
  if (sec_buffer_info_.private_data)
    buffer_allocator_->FreeBuffer(&sec_buffer_info_);
}

bool HWCBWCheckerLayer::InitInternal()
{
  bw_rgb888_ = 0;
  HWCDebugHandler::Get()->GetProperty("persist.vendor.display.pxlw.bw_rgb888", &bw_rgb888_);

  buffer_info_.buffer_config.width = width_;
  buffer_info_.buffer_config.height = height_;
  if (primary_ || bw_rgb888_)
    buffer_info_.buffer_config.format = kFormatRGB888;
  else
    buffer_info_.buffer_config.format = kFormatRGB565;
  buffer_info_.buffer_config.buffer_count = 1;

  int ret = buffer_allocator_->AllocateBuffer(&buffer_info_);
  if (ret != 0) {
    DLOGE("Buffer allocation failed. ret: %d", ret);
    return false;
  }

  sec_buffer_info_.buffer_config.width = width_;
  sec_buffer_info_.buffer_config.height = height_;
  if (primary_ || bw_rgb888_)
    sec_buffer_info_.buffer_config.format = kFormatRGB888;
  else
    sec_buffer_info_.buffer_config.format = kFormatRGB565;
  sec_buffer_info_.buffer_config.buffer_count = 1;

  ret = buffer_allocator_->AllocateBuffer(&sec_buffer_info_);
  if (ret != 0) {
    DLOGE("Second Buffer allocation failed. ret: %d", ret);
    return false;
  }

  void *buf_addr = mmap(NULL, buffer_info_.alloc_buffer_info.size,
                        PROT_READ | PROT_WRITE, MAP_SHARED,
                        buffer_info_.alloc_buffer_info.fd, 0);
  if (buf_addr == MAP_FAILED) {
    DLOGE("mmap failed. err = %d", errno);
    return false;
  }
  uint8_t *buffer = static_cast<uint8_t *>(buf_addr);

  GenerateBWChecker(buffer);

  if (munmap(buf_addr, buffer_info_.alloc_buffer_info.size) != 0) {
    DLOGE("munmap failed. err = %d", errno);
    return false;
  }

  layer_ = new Layer();
  layer_->src_rect = LayerRect(0, 0, width_, height_);
  layer_->dst_rect = layer_->src_rect;
  layer_->frame_rate = 1;
  layer_->blending = kBlendingPremultiplied;
  // the single buffer flag makes SDE think it's always updating
  // and thus the layer is kept on a seperate hardware layer
  layer_->flags.single_buffer = 1;
  layer_->visible_regions.push_back(layer_->dst_rect);

  layer_->input_buffer.unaligned_width = width_;
  layer_->input_buffer.unaligned_height = height_;
  layer_->input_buffer.width = buffer_info_.alloc_buffer_info.aligned_width;
  layer_->input_buffer.height = buffer_info_.alloc_buffer_info.aligned_height;
  layer_->input_buffer.size = buffer_info_.alloc_buffer_info.size;
  layer_->input_buffer.planes[0].fd = buffer_info_.alloc_buffer_info.fd;
  layer_->input_buffer.planes[0].stride = buffer_info_.alloc_buffer_info.stride;
  layer_->input_buffer.format = buffer_info_.buffer_config.format;
  layer_->input_buffer.buffer_id = 0;

  DLOGI("Input buffer WxH %dx%d format %s size %d fd %d stride %d", layer_->input_buffer.width,
        layer_->input_buffer.height, GetFormatString(layer_->input_buffer.format),
        layer_->input_buffer.size, layer_->input_buffer.planes[0].fd,
        layer_->input_buffer.planes[0].stride);

  return true;
}

Layer* HWCBWCheckerLayer::GetLayer(int id, int type)
{
  if (PxlwIrisWrapper::GetInstance()->HasIris()) {
    if (type == kPrimary && id != HWC_DISPLAY_PRIMARY) {
      return layer_;
    }
  }
  return NULL;
}

static inline void PixelCopy(uint32_t red, uint32_t green, uint32_t blue, uint32_t alpha,
                             uint8_t **buffer, bool bits24)
{
  if (bits24) {
    *(*buffer)++ = UINT8(red & 0xFF);
    *(*buffer)++ = UINT8(green & 0xFF);
    *(*buffer)++ = UINT8(blue & 0xFF);
  } else {
    *(*buffer)++ = UINT8((red & 0x1F)<<3 | (green & 0x3F)>>3);
    *(*buffer)++ = UINT8((green & 0x3F)<<5 | (blue & 0x1F));
  }
}

void HWCBWCheckerLayer::GenerateBWChecker(uint8_t *buffer)
{
  uint32_t width = buffer_info_.buffer_config.width;
  uint32_t height = buffer_info_.buffer_config.height;
  uint32_t aligned_width = buffer_info_.alloc_buffer_info.aligned_width;
  uint32_t buffer_stride;
  uint32_t bits_per_component = 8;
  uint32_t max_color_val = (1 << bits_per_component) - 1;
  bool bits24 = buffer_info_.buffer_config.format == kFormatRGB888;
  bool big_pattern = property_get_bool("debug.iris.big_pattern", false);
  if (bw_rgb888_)
    max_color_val = bw_rgb888_;

  if (bits24)
    buffer_stride = aligned_width * 3;
  else
    buffer_stride = aligned_width * 2;
  for (uint32_t loop_height = 0; loop_height < height; loop_height++) {
    uint32_t color = big_pattern ? ((loop_height / 16) % 2): loop_height % 2;
    uint8_t *temp = buffer + (loop_height * buffer_stride);

    for (uint32_t loop_width = 0; loop_width < width; loop_width++) {
      if (color == 0) { // white
        PixelCopy(max_color_val, max_color_val, max_color_val, 0, &temp, bits24);
      }
      if (color == 1) { // black
        PixelCopy(0, 0, 0, 0, &temp, bits24);
      }
      if (big_pattern) {  // test, big checker pattern
        if (loop_width % 16 == 15)
          color = (color + 1) % 2;
      } else {
        color = (color + 1) % 2;
      }
    }
  }
}

void HWCBWCheckerLayer::GenerateNextBufferData(uint8_t *buffer, uint32_t pattern) {
  uint32_t width = buffer_info_.buffer_config.width;
  uint32_t height = buffer_info_.buffer_config.height;
  uint32_t aligned_width = buffer_info_.alloc_buffer_info.aligned_width;
  uint32_t buffer_stride;
  uint32_t lh = 0; // loop height
  uint32_t lw = 0; // loop width
  uint8_t idx = (pattern >> 24) & 0xff;
  uint8_t r = (pattern >> 16) & 0xff;
  uint8_t g = (pattern >> 8) & 0xff;
  uint8_t b = pattern & 0xff;
  bool bits24 = buffer_info_.buffer_config.format == kFormatRGB888;

  struct size_map {
    uint8_t up_bound;
    uint8_t down_bound;
  };
  struct size_map map[] = {
      { 24, 75 },
      { 14, 86 },
      { 3, 97 },
      { 0, 100 }
  };
  struct size_map mapw[] = {
      { 25, 75 },
      { 15, 85 },
      { 10, 90 },
      { 0, 100 }
  };
  idx %= sizeof(map)/sizeof(size_map);

  if (bits24)
    buffer_stride = aligned_width * 3;
  else
    buffer_stride = aligned_width * 2;

  for (lh = 0; lh < height*map[idx].up_bound/100; lh++) {
    uint8_t *temp = buffer + (lh * buffer_stride);

    for (lw = 0; lw < width; lw++) {
      PixelCopy(0, 0, 0, 0, &temp, bits24);
    }
  }

  for (; lh < height*map[idx].down_bound/100; lh++) {
    uint8_t *temp = buffer + (lh * buffer_stride);

    for (lw = 0; lw < width*mapw[idx].up_bound/100; lw++) {
      PixelCopy(0, 0, 0, 0, &temp, bits24);
    }
    for (; lw < width*mapw[idx].down_bound/100; lw++) {
      PixelCopy(r, g, b, 0, &temp, bits24);
    }
    for (; lw < width; lw++) {
      PixelCopy(0, 0, 0, 0, &temp, bits24);
    }
  }

  for (; lh < height; lh++) {
    uint8_t *temp = buffer + (lh * buffer_stride);

    for (lw = 0; lw < width; lw++) {
      PixelCopy(0, 0, 0, 0, &temp, bits24);
    }
  }
}

int HWCBWCheckerLayer::GenerateNextBuffer(uint32_t pattern) {
  int next_buf_idx = (cur_buf_idx_ + 1) % 2;
  BufferInfo *next_buffer_info;
  if (next_buf_idx == 0)
    next_buffer_info = &buffer_info_;
  else
    next_buffer_info = &sec_buffer_info_;

  void *buf_addr = mmap(NULL, next_buffer_info->alloc_buffer_info.size,
                        PROT_READ | PROT_WRITE, MAP_SHARED,
                        next_buffer_info->alloc_buffer_info.fd, 0);
  if (buf_addr == MAP_FAILED) {
    DLOGE("mmap failed. err = %d", errno);
    return -1;
  }
  uint8_t *buffer = static_cast<uint8_t *>(buf_addr);

  GenerateNextBufferData(buffer, pattern);

  if (munmap(buf_addr, next_buffer_info->alloc_buffer_info.size) != 0) {
    DLOGE("munmap failed. err = %d", errno);
    return -1;
  }

  return 0;
}

int HWCBWCheckerLayer::SetCaliPattern(void *self, uint32_t pattern)
{
  int ret = 0;
  HWCBWCheckerLayer *hwc_bw_layer = static_cast<HWCBWCheckerLayer *>(self);
  DLOGI("%x", pattern);
  if (pattern == 0xffffffff) {
    // turn off pattern display
    hwc_bw_layer->display_cali_pattern_ = false;
    return ret;
  }
  hwc_bw_layer->lock_.lock();
  if (hwc_bw_layer->request_update_layer_ == true) {
    DLOGI("Last calibration pattern isn't displayed!");
    hwc_bw_layer->request_update_layer_ = false;
  }
  hwc_bw_layer->lock_.unlock();
  ret = hwc_bw_layer->GenerateNextBuffer(pattern);
  hwc_bw_layer->lock_.lock();
  if (ret == 0) {
    hwc_bw_layer->display_cali_pattern_ = true;
    hwc_bw_layer->request_update_layer_ = true;
  }
  hwc_bw_layer->lock_.unlock();
  return ret;
}

bool HWCBWCheckerLayer::IsCaliPatternEnable() {
  SetCaliPatternCB();
  if (display_cali_pattern_)
    UpdateLayer();
  return display_cali_pattern_;
}

void HWCBWCheckerLayer::UpdateLayer() {
  if (display_cali_pattern_) {
    lock_.lock();
    if (request_update_layer_) {
      cur_buf_idx_ = (cur_buf_idx_ + 1) % 2;
      if (cur_buf_idx_ == 0)
        layer_->input_buffer.planes[0].fd = buffer_info_.alloc_buffer_info.fd;
      else
        layer_->input_buffer.planes[0].fd = sec_buffer_info_.alloc_buffer_info.fd;
      request_update_layer_ = false;
    }
    lock_.unlock();
  }
}

void HWCBWCheckerLayer::SetCaliPatternCB() {
  if (set_cali_pattern_cb_ == false) {
    if (primary_ && PxlwIrisWrapper::GetInstance()->HasSoftIris()) {
      PxlwIrisWrapper::GetInstance()->SetCaliPatternCB(&SetCaliPattern, this);
      set_cali_pattern_cb_ = true;
      DLOGI("SetCaliPatternCB OK");
    }
  }
}

void HWCBWCheckerLayer::ReleaseFence() {
}

}

#endif //PXLW_IRIS
