/*
 * Copyright (c) 2012-2013, 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright (C) 2020, Pixelworks, Inc.
 * These files include source code that is provided
 * under the terms and conditions of a Master Software License Agreement and
 * its applicable schedules.
 */

#ifndef __HWC_BWCHECKER_LAYER__
#define __HWC_BWCHECKER_LAYER__

#include <utils/Mutex.h>
//#include "hwc_display.h"
#include "hwc_layers.h"
#include "hwc_buffer_allocator.h"

namespace sdm {

using namespace android;

class HWCBWCheckerLayer : public RefBase {
 public:
  HWCBWCheckerLayer(BufferAllocator *buffer_allocator, uint32_t width, uint32_t height, bool primary);
  ~HWCBWCheckerLayer();
  bool Init();
  Layer *GetLayer(int id, int type);
  bool IsCaliPatternEnable();
  void ReleaseFence();
  bool SetFrameBufferResolution(uint32_t width, uint32_t height);

 private:
  BufferAllocator *buffer_allocator_ = nullptr;
  uint32_t width_;
  uint32_t height_;
  BufferInfo buffer_info_ = {};
  BufferInfo sec_buffer_info_ = {};
  Layer *layer_;
  int bw_rgb888_;

  bool primary_ = false;
  bool set_cali_pattern_cb_ = false;
  int cur_buf_idx_ = 0;
  bool display_cali_pattern_ = false;
  bool request_update_layer_ = false;
  android::Mutex lock_;

  void UpdateLayer();
  void SetCaliPatternCB();

  void GenerateBWChecker(uint8_t *buffer);
  void GenerateNextBufferData(uint8_t *buffer, uint32_t pattern);
  int GenerateNextBuffer(uint32_t pattern);
  static int SetCaliPattern(void *self, uint32_t pattern);
  void Release();
  bool InitInternal();
};

}  // namespace sdm

#endif  // __HWC_BWCHECKER_LAYER__
