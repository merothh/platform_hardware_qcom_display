/*
 * Copyright (c) 2012-2013, 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright (C) 2020, Pixelworks, Inc.
 * These files include source code that is provided
 * under the terms and conditions of a Master Software License Agreement and
 * its applicable schedules.
 */

virtual HWC2::Error MoveLayer(hwc2_layer_t layer_id, HWCDisplay *dest);
virtual HWC2::Error MoveLayers(HWCDisplay *dest, hwc2_layer_t except_layer_id);
virtual HWC2::Error MoveLayersByZ(HWCDisplay *dest, uint32_t zorder, bool dir);
virtual bool HasLayer(hwc2_layer_t layer_id);
virtual bool LayerIsEmpty(HWCDisplay *dest);
virtual bool IsLayerBufferChanged(hwc2_layer_t layer_id, buffer_handle_t buffer);
virtual void SetMoveLayersDelay(bool delay);
virtual HWC2::Composition GetLayerDeviceSelectedCompositionType(hwc2_layer_t layer_id);
virtual HWC2::Composition GetLayerClientRequestedCompositionType(hwc2_layer_t layer_id);
virtual bool IsGPUTargetComp();
virtual HWC2::Transform LayerTransform2Transform(LayerTransform layer_transform);
bool move_layers_delay_ = false;
virtual int32_t GetLayerZOrder(hwc2_layer_t layer_id);
HWC2::Error CopyColorTransform(HWCDisplay *dest);
HWC2::Error SyncColorTransform(float *matrix);
virtual bool hasClientComposition();
void CreateBWLayer();
bool CheckMoveLayersDelay(uint32_t *out_num_elements);
bool CheckCommitLayerStack();
