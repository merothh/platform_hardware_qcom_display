/*
 * Copyright (c) 2012-2013, 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright (C) 2020, Pixelworks, Inc.
 * These files include source code that is provided
 * under the terms and conditions of a Master Software License Agreement and
 * its applicable schedules.
 */

void DisplayBase::UpdateLayerPipes() {
  if (!comp_manager_ /*|| !comp_manager_->HasIris5Dual()*/) {
    return;
  }

  if (!IsPrimaryDisplay() && transform_csc_update) {
    uint32_t hw_layers_count = hw_layers_.info.hw_layers.size();
    uint32_t i;
    DLOGI_IF(kTagDriverConfig, "set CSC NoOp size: %d", hw_layers_count);
    for (i = 0; i < hw_layers_count; i++) {
      HWLayerConfig &layer_config = hw_layers_.config[i];
      Layer &hw_layer = hw_layers_.info.hw_layers.at(i);
      LayerBuffer *input_buffer = &hw_layer.input_buffer;
      if (input_buffer->format != kFormatRGB565) {
        DLOGI_IF(kTagDriverConfig, "layer[%d] set CSC NoOp", i);
        //input_buffer->flags.dual_channel_layer = true;
        layer_config.left_pipe.dgm_csc_info.op = kNoOp;
        layer_config.right_pipe.dgm_csc_info.op = kNoOp;
      }
    }
  }
}
