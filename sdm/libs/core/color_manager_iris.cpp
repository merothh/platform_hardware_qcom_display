/*
 * Copyright (c) 2012-2013, 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright (C) 2020, Pixelworks, Inc.
 * These files include source code that is provided
 * under the terms and conditions of a Master Software License Agreement and
 * its applicable schedules.
 */

DisplayError STCIntfClient::OpenStcLibrary() {
  bool opened = false;
  if (has_softiris_) {
    opened = stc_intf_lib_.Open("libpwsnapdragoncolor.so");
  }
  if (!opened && !stc_intf_lib_.Open(kStcIntfLib_)) {
    DLOGW("IRIS_LOG_HWC STC library is not present");
    return kErrorNotSupported;
  }

  if (opened) {
    has_softiris_ = true;
  }
  return opened ? kErrorNone : kErrorVersion;
}

DisplayError STCIntfClient::SetupSoftIrisLibrary(const std::string &panel_name) {
  bool result;
  if (!has_softiris_) {
     return kErrorNotSupported;
  }

  if (!pxlw_softiris_lib_.Open("libpwirissoft.so")) {
    DLOGW("IRIS_LOG_HWC Soft Iris library is not present");
    return kErrorNotSupported;
  }

  result = pxlw_softiris_lib_.Sym("pxlwIrisCreate", reinterpret_cast<void **>(&pxlwIrisCreate));
  result &= pxlw_softiris_lib_.Sym("pxlwIrisDestroy", reinterpret_cast<void **>(&pxlwIrisDestroy));
  result &= pxlw_softiris_lib_.Sym("pxlwIrisCommit", reinterpret_cast<void **>(&pxlwIrisCommit));
  result &= pxlw_softiris_lib_.Sym("pxlwIrisSetDisplayMode", reinterpret_cast<void **>(&pxlwIrisSetDisplayMode));

  if (result) {
    pxlwIrisCreate(panel_name, stc_intf_);
    return kErrorNone;
  } else {
    DLOGW("IRIS_LOG_HWC Get Soft Iris functions failed");
    return kErrorNotSupported;
  }
}

bool ColorManagerProxy::HasSoftIris() {
  return (stc_intf_client_ && stc_intf_client_->has_softiris_);
}

void ColorManagerProxy::SetDynamicRange(int32_t dynamic_range) {
  if (stc_intf_client_ && stc_intf_client_->pxlwIrisSetDisplayMode && kBuiltIn == device_type_) {
    // low 16 bits is color mode; high 16 bits is dynamic range
    stc_intf_client_->pxlwIrisSetDisplayMode(NULL, -1, (dynamic_range << 16));
  }
}
