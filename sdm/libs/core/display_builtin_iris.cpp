/*
 * Copyright (c) 2012-2013, 2016, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Copyright (C) 2020, Pixelworks, Inc.
 * These files include source code that is provided
 * under the terms and conditions of a Master Software License Agreement and
 * its applicable schedules.
 */

DisplayError DisplayBuiltIn::SetIrisHdrMode(int32_t mode) {
  iris_hdr_mode = mode;
  return kErrorNone;
}

DisplayError DisplayBuiltIn::SetColorTransform(const uint32_t length, const double *color_transform) {
  DisplayError ret = DisplayBase::SetColorTransform(length, color_transform);
  /*if (ret == kErrorNone && comp_manager_ && comp_manager_->HasIris5Dual()) {
    uint32_t i = 0;
    for (; i < length; i++) {
      transform_csc[i] = color_transform[i];
    }
    transform_csc_update = true;
  }
*/
  return ret;
}

static uint64_t CSCCoeffGet(double f_value) {
  double abs_value = fabs(f_value);
  uint64_t low_value = (uint64_t)(abs_value * 0.9 * 0x2000000);
  uint64_t result = 0;
  if (low_value == 0) {
    return 0;
  }
  if (f_value < 0) {
    result = (~low_value) + 1;
  } else {
    result = low_value;
  }
  return (result & 0x7FFFFFFFFF);
}

void DisplayBuiltIn::UpdateColorTransform() {
  if (!comp_manager_ /*|| !comp_manager_->HasIris5Dual()*/) {
    return;
  }

  if (!IsPrimaryDisplay() && transform_csc_update) {
    // The secondary channel
    uint32_t hw_layers_count = hw_layers_.info.hw_layers.size();
    uint32_t i;
    for (i = 1; i < hw_layers_count; i++) {
      HWLayerConfig &layer_config = hw_layers_.config[i];
      Layer &hw_layer = hw_layers_.info.hw_layers.at(i);
      LayerBuffer *input_buffer = &hw_layer.input_buffer;
      if (input_buffer->format != kFormatRGB565) {
        //input_buffer->flags.dual_channel_layer = true;
        layer_config.left_pipe.dgm_csc_info.op = kSet;
        layer_config.left_pipe.inverse_pma_info.op = kNoOp;
        layer_config.left_pipe.dgm_csc_info.csc.ctm_coeff[0] = CSCCoeffGet(transform_csc[0]);
        layer_config.left_pipe.dgm_csc_info.csc.ctm_coeff[1] = CSCCoeffGet(transform_csc[4]);
        layer_config.left_pipe.dgm_csc_info.csc.ctm_coeff[2] = CSCCoeffGet(transform_csc[8]);
        layer_config.left_pipe.dgm_csc_info.csc.ctm_coeff[3] = CSCCoeffGet(transform_csc[1]);
        layer_config.left_pipe.dgm_csc_info.csc.ctm_coeff[4] = CSCCoeffGet(transform_csc[5]);
        layer_config.left_pipe.dgm_csc_info.csc.ctm_coeff[5] = CSCCoeffGet(transform_csc[9]);
        layer_config.left_pipe.dgm_csc_info.csc.ctm_coeff[6] = CSCCoeffGet(transform_csc[2]);
        layer_config.left_pipe.dgm_csc_info.csc.ctm_coeff[7] = CSCCoeffGet(transform_csc[6]);
        layer_config.left_pipe.dgm_csc_info.csc.ctm_coeff[8] = CSCCoeffGet(transform_csc[10]);

        layer_config.left_pipe.dgm_csc_info.csc.pre_bias[0] = 0;
        layer_config.left_pipe.dgm_csc_info.csc.pre_bias[1] = 0;
        layer_config.left_pipe.dgm_csc_info.csc.pre_bias[2] = 0;
        layer_config.left_pipe.dgm_csc_info.csc.post_bias[0] = 0;
        layer_config.left_pipe.dgm_csc_info.csc.post_bias[1] = 0;
        layer_config.left_pipe.dgm_csc_info.csc.post_bias[2] = 0;

        layer_config.left_pipe.dgm_csc_info.csc.pre_clamp[0] = 0;
        layer_config.left_pipe.dgm_csc_info.csc.pre_clamp[1] = 0x3ff;
        layer_config.left_pipe.dgm_csc_info.csc.pre_clamp[2] = 0;
        layer_config.left_pipe.dgm_csc_info.csc.pre_clamp[3] = 0x3ff;
        layer_config.left_pipe.dgm_csc_info.csc.pre_clamp[4] = 0;
        layer_config.left_pipe.dgm_csc_info.csc.pre_clamp[5] = 0x3ff;
        layer_config.left_pipe.dgm_csc_info.csc.post_clamp[0] = 0;
        layer_config.left_pipe.dgm_csc_info.csc.post_clamp[1] = 0x3ff;
        layer_config.left_pipe.dgm_csc_info.csc.post_clamp[2] = 0;
        layer_config.left_pipe.dgm_csc_info.csc.post_clamp[3] = 0x3ff;
        layer_config.left_pipe.dgm_csc_info.csc.post_clamp[4] = 0;
        layer_config.left_pipe.dgm_csc_info.csc.post_clamp[5] = 0x3ff;
        if (layer_config.right_pipe.valid) {
          layer_config.right_pipe.dgm_csc_info.op = kSet;
          layer_config.right_pipe.inverse_pma_info.op = kNoOp;
          memcpy(layer_config.right_pipe.dgm_csc_info.csc.ctm_coeff, layer_config.left_pipe.dgm_csc_info.csc.ctm_coeff, MAX_CSC_MATRIX_COEFF_SIZE*sizeof(int64_t));
          memcpy(layer_config.right_pipe.dgm_csc_info.csc.pre_bias, layer_config.left_pipe.dgm_csc_info.csc.pre_bias, MAX_CSC_BIAS_SIZE*sizeof(uint32_t));
          memcpy(layer_config.right_pipe.dgm_csc_info.csc.post_bias, layer_config.left_pipe.dgm_csc_info.csc.post_bias, MAX_CSC_BIAS_SIZE*sizeof(uint32_t));
          memcpy(layer_config.right_pipe.dgm_csc_info.csc.pre_clamp, layer_config.left_pipe.dgm_csc_info.csc.pre_clamp, MAX_CSC_CLAMP_SIZE*sizeof(uint32_t));
          memcpy(layer_config.right_pipe.dgm_csc_info.csc.post_clamp, layer_config.left_pipe.dgm_csc_info.csc.post_clamp, MAX_CSC_CLAMP_SIZE*sizeof(uint32_t));
        }
      }
    }
    // transform csc matrix should be set everytime.
    // transform_csc_update = false;
  }
}

bool DisplayBuiltIn::IsGPUTargetComp() {
  uint32_t num_hw_layers = 0;
  uint32_t layer_index;
  Layer *sdm_layer;

  if (!comp_manager_ /*|| !comp_manager_->HasIris5Dual()*/) {
    return false;
  }

  if (hw_layers_.info.stack) {
    num_hw_layers = UINT32(hw_layers_.info.hw_layers.size());
  }

  if (num_hw_layers == 1) {
    layer_index = hw_layers_.info.index.at(0);
    sdm_layer = hw_layers_.info.stack->layers.at(layer_index);
    return sdm_layer->composition == kCompositionGPUTarget;
  } else if (num_hw_layers == 2) {
    layer_index = hw_layers_.info.index.at(0);
    sdm_layer = hw_layers_.info.stack->layers.at(layer_index);
    if (sdm_layer->flags.solid_fill) {
      layer_index = hw_layers_.info.index.at(1);
      sdm_layer = hw_layers_.info.stack->layers.at(layer_index);
      if (sdm_layer->composition == kCompositionGPUTarget) {
        DLOGD("bottom is solid color, the 2nd layer is gpu_target");
        return true;
      }
    }
  }

  return false;
}

void DisplayBuiltIn::SyncColorTransform(const uint32_t length, const double *color_transform) {
  if (!comp_manager_ /*|| !comp_manager_->HasIris5Dual()*/) {
    return;
  }

  uint32_t i = 0;
  for (; i < length; i++) {
    transform_csc[i] = color_transform[i];
  }
  DLOGI_IF(kTagNone, "transform_csc[0]: %f", transform_csc[0]);
  transform_csc_update = true;
}
